package com.inobitec;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.inobitec.model.Patient;
import com.inobitec.sort.PatientComparator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {


    public static void main(String[] args) {
        XmlMapper xmlMapper = new XmlMapper();
        String sortBy = "";
        List<Patient> patients = new ArrayList<>();
        xmlMapper.registerModule(new JSR310Module());

        for (String filePath : args) {
            if (!filePath.endsWith(".xml")) {
                sortBy = filePath;
            } else {
                File file = new File(filePath);
                try {
                    patients.addAll(xmlMapper.readValue(file, new TypeReference<List<Patient>>() {
                    }));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (patients.isEmpty()) {
            System.out.println("Файл не введен");
            System.exit(1);
        }
        if (!sortBy.isEmpty()) {
            patients.sort(PatientComparator.getComparator(PatientComparator.valueOf(sortBy)));
        }
        System.out.format("%15s %23s %7s %11s \n", "ФИО", "Возраст", "Пол", "Телефон");
        patients.forEach(patient -> System.out.format("|%s %-10s %-10s |%6s |%8s |%18s| \n",
                patient.getLastName(),
                patient.getFirstName(),
                patient.getMiddleName(),
                patient.getAge(),
                patient.getGender(),
                patient.getPhone()));
    }
}