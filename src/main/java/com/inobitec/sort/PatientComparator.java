package com.inobitec.sort;

import com.inobitec.model.Patient;

import java.util.Comparator;

public enum PatientComparator implements Comparator<Patient> {
    name {
        public int compare(Patient patient1, Patient patient2) {
            return patient1.getFirstName().compareTo(patient2.getFirstName());
        }
    },
    age {
        public int compare(Patient patient1, Patient patient2) {
            return patient1.getBirthday().compareTo(patient2.getBirthday());
        }
    };

    public static Comparator<Patient> getComparator(final PatientComparator comparator) {
        return new Comparator<Patient>() {
            public int compare(Patient patient1, Patient patient2) {
                return comparator.compare(patient1, patient2);
            }
        };
    }
}

