package com.inobitec.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Data
public class Patient {

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("last_name")
    private String lastName;

    private LocalDate birthday;
    private String gender;
    private String phone;

    public long getAge() {
        return ChronoUnit.YEARS.between(birthday, LocalDate.now());
    }


}
